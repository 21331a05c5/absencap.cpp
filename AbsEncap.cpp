#include<iostream>
using namespace std;
class AccessSpecifierDemo{
   private:
      int priVar;
   protected:
      int proVar;
   public:
      int pubVar;
  
   public:
    void setVar(int priValue, int proValue, int pubValue){
       priVar=priValue;
       proVar=proValue;
       pubVar=pubValue;
   }
   public:
    void getVar(){
       cout<<"\nPrivate: "<<priVar<<endl;
       cout<<"\nProtected: "<<proVar<<endl;
       cout<<"\nPublic: "<<pubVar<<endl;
   }
 }; 
int main(){
       AccessSpecifierDemo obj;
       obj.setVar(1, 2, 3);
       obj.getVar();
   
}
